package es.upm.ctb.midas.clikes.negation;

public interface StringMapResource {
	
	public String get(String aKey);

}
